import unicodedata
import string

valid_filename_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
char_limit = 255


# Utility to generate safe filename
def safe_filename(filename: str, whitelist: str = valid_filename_chars, replace: str = " ") -> str:
    # replace spaces
    for r in replace:
        filename = filename.replace(r, "_")

    # keep only valid ascii chars
    cleaned_filename = (
        unicodedata.normalize("NFKD", filename)
        .encode("ASCII", "ignore")
        .decode()
    )

    # keep only whitelisted chars
    cleaned_filename = "".join(c for c in cleaned_filename if c in whitelist)
    if len(cleaned_filename) > char_limit:
        print(
            """
            Warning, filename truncated because it was over {}.
            Filenames may no longer be unique
            """.format(
                char_limit
            )
        )
    return cleaned_filename[:char_limit]
