from datetime import datetime

from scrapy import Request

from kawalcovid19.spiders.base import BaseSpider


class Liputan6(BaseSpider):
    name = "liputan6"
    allowed_domains = ["liputan6.com"]

    def start_requests(self):
        today = datetime.today().strftime("%d/%m/%Y")
        url = """
            https://www.liputan6.com/search?order=
            latest&channel_id=&from_date={date}&to_
            date={date}&type=all&q=corona""".format(
            date=today
        )
        yield Request(url=url)

    def parse(self, response):
        links = response.xpath(
            '//a[@class="ui--a articles--iridescent-list--text-item__title-link"]/@href'
        ).getall()
        for link in links:
            yield Request(url=link, callback=self.parse_article)

    def parse_article(self, response):
        title = response.xpath(
            '//h1[@class="read-page--header--title entry-title"]/text()'
        ).get()
        author = response.xpath(
            '//span[@class="read-page--header--author__name fn"]/text()'
        ).get()
        date = response.xpath(
            '//time[@class="read-page--header--author__datetime updated"]/text()'
        ).get()
        content = "\n".join(
            response.xpath(
                '//div[@class="article-content-body__item-content"]//text()'
            ).getall()
        ).strip()

        return self.to_item_loader(response, title, content, author, date)
