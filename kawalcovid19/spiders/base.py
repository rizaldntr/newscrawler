import os

import scrapy
from scrapy.loader import ItemLoader
from azure.storage.blob import ContainerClient
from scrapy import signals, crawler as crawler_type
from scrapy.exceptions import NotConfigured
from scrapy.http import Response
from slacker import Slacker

from kawalcovid19.items import NewsItem

# Configurations
AZURE_BLOB_STORAGE_CONN_STRING = os.getenv(
    "AZURE_BLOB_STORAGE_CONN_STRING", "default"
)
SLACK_TOKEN = os.getenv("SLACK_TOKEN", "")


class BaseSpider(scrapy.Spider):
    # Initialize connection to Azure Blob Storage
    def __init__(self) -> None:
        # Check the configuration first
        if AZURE_BLOB_STORAGE_CONN_STRING == "default":
            raise NotConfigured(
                "AZURE_BLOB_STORAGE_CONN_STRING is not configured"
            )

        # Configure container to save JSON files
        self.azure_json_container = ContainerClient.from_connection_string(
            conn_str=AZURE_BLOB_STORAGE_CONN_STRING,
            container_name="crawler-dataset",
        )

        # Configure container for BasicAI dataset
        self.azure_basicai_container = ContainerClient.from_connection_string(
            conn_str=AZURE_BLOB_STORAGE_CONN_STRING,
            container_name="basicai-dataset",
        )

        if SLACK_TOKEN != "":
            self.is_slack = True
            self.slack = Slacker(SLACK_TOKEN)
        else:
            self.is_slack = False
            self.logger.info("Post error to #crawler-errors is disabled")

    # Capture the signal spider_opened and spider_closed
    # https://doc.scrapy.org/en/latest/topics/signals.html
    @classmethod
    def from_crawler(cls, crawler: crawler_type, *args, **kwargs) -> 'BaseSpider':
        spider = super(BaseSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(
            spider.spider_opened, signal=signals.spider_opened
        )
        crawler.signals.connect(
            spider.spider_closed, signal=signals.spider_closed
        )
        return spider

    def spider_opened(self, _spider: 'BaseSpider') -> None:
        self.logger.info("Spider opened")

    def spider_closed(self, spider: 'BaseSpider', reason: str) -> None:
        spider.logger.info("Spider closed: {} {}".format(spider.name, reason))
        # if spider finished without error update last_scraped_at
        if reason == "finished":
            self.logger.info("Spider {} finished".format(spider.name))
        else:
            if self.is_slack:
                # Send error to slack
                error_msg = "{}: Spider fail because: {}".format(
                    spider.name, reason
                )
                self.slack.chat.post_message(
                    "#crawler-error", error_msg, as_user=True
                )

    # subscibe to item_droped event
    def item_dropped(self, _item: NewsItem, _response: Response, exception: Exception, spider: 'BaseSpider') -> None:
        if self.is_slack:
            # Send error to slack
            error_msg = "{}: Item dropped because: {}".format(
                spider.name, exception
            )
            spider.slack.chat.post_message(
                "#crawler-error", error_msg, as_user=True
            )

    def to_item_loader(self, response, title, content, author, published_at):
        article = ItemLoader(item=NewsItem(), response=response)
        article.add_value("url", response.url)
        article.add_value("title", title)
        article.add_value("author", author)
        article.add_value("content", content)
        article.add_value("published_at", published_at)
        article.add_value("news_site", self.name)
        return article.load_item()