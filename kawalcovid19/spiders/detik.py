from scrapy import Request
from scrapy.loader import ItemLoader
from urllib.parse import urlparse

from kawalcovid19.items import NewsItem
from kawalcovid19.spiders.base import BaseSpider


class DetikSpider(BaseSpider):
    name = "detik"
    allowed_domains = ["detik.com"]
    start_urls = [
        "https://www.detik.com/tag/virus-corona",
        "https://www.detik.com/tag/corona",
    ]

    custom_settings = {
        "USER_AGENT": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"
    }

    def parse(self, response):
        article_links = response.xpath(
            '//div[contains(@class, "list-berita")]/article/a/@href'
        ).getall()

        for article_link in article_links:
            yield Request(url=article_link, callback=self.parse_article)

    def parse_article(self, response):
        url = urlparse(response.request.url)
        netloc = url.netloc  # base url
        self.logger.info("parse_news: {}".format(response))

        if "finance.detik.com" == netloc:
            return self.parse_article_detik_finance(response)
        if "news.detik.com" == netloc:
            return self.parse_article_detik_news(response)

        self.logger.warn(
            "{} base url is not supported for crawler {}, full url: {}".format(
                netloc, self.name, url
            )
        )

    def _to_item_loader(self, response, title, content, author, published_at):
        article = ItemLoader(item=NewsItem(), response=response)
        article.add_value("url", response.url)
        article.add_value("title", title)
        article.add_value("author", author)
        article.add_value("content", content)
        article.add_value("published_at", published_at)
        article.add_value("news_site", self.name)
        return article.load_item()

    def parse_article_detik_news(self, response):
        return self._to_item_loader(
            response=response,
            title=response.xpath(
                '//h1[contains(@class, "detail__title")]/text()'
            )
            .get()
            .strip(),
            content="\n".join(
                response.xpath(
                    '//div[contains(@class, "detail__body")]/p/text()'
                ).extract()
            ).strip(),
            author=response.xpath(
                '//div[contains(@class, "detail__author")]/text()'
            )
            .get()
            .strip(),
            published_at=response.xpath(
                '//div[contains(@class, "detail__date")]/text()'
            )
            .get()
            .strip(),
        )

    def parse_article_detik_finance(self, response):
        title_part = response.xpath('//div[contains(@class, "jdl")]')
        body_part = response.xpath('//div[contains(@id, "detikdetailtext")]')
        return self._to_item_loader(
            response=response,
            title=title_part.xpath("//h1/text()").get().strip(),
            content="\n".join(body_part.xpath("//p/text()").extract()).strip(),
            author=title_part.xpath('//div[contains(@class, "author")]/text()')
            .get()
            .strip(),
            published_at=title_part.xpath(
                '//div[contains(@class, "date")]/text()'
            )
            .get()
            .strip(),
        )

