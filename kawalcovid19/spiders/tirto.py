from scrapy import Request
from scrapy.shell import inspect_response

from kawalcovid19.items import NewsItem
from kawalcovid19.spiders.base import BaseSpider

class TirtoSpider(BaseSpider):
    name = "tirto"
    base_url = "https://tirto.id"
    allowed_domains = ["tirto.id"]
    start_urls = [
        "https://tirto.id/search?q=corona"
    ]

    def parse(self, response):
        links = response.xpath(
            '//div[@class="col-md-4 mb-4 news-list-fade"]//a/@href'
        ).getall()

        for link in links:
            url = "{}/{}".format(self.base_url, link[2:])
            yield Request(url=url, callback=self.parse_article)

        next_pages = response.xpath('//li[@class="pagination-item"]/a/@href').getall()

        for next_page in next_pages:
            url = "{}{}".format(self.base_url, next_page)
            yield Request(url=url, callback=self.parse)

    def parse_article(self, response):
        title = response.xpath('//h1[@class="news-detail-title text-center animated zoomInUp my-3"]/text()').get()
        author, date = response.xpath('//span[@class="detail-date mt-1 text-left"]/text()').get()[5:].split("-")
        author = author.strip()
        date = date.strip()
        content = "\n".join(response.xpath('//div[@class="content-text-editor"][2]//text()').getall()).strip()
        
        return self.to_item_loader(response, title, content, author, date)