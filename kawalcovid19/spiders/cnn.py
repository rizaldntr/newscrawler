from datetime import datetime

from scrapy import Request

from kawalcovid19.spiders.base import BaseSpider


class CNNSpider(BaseSpider):
    name = "CNN"
    allowed_domains = ["cnnindonesia.com"]
    base_url = "https://www.cnnindonesia.com/search/?query=corona&kanal=nasional&date={date}&p={page}"
    today = datetime.today().strftime("%d/%m/%Y")
    page = 1

    def start_requests(self):
        url = self.base_url.format(date=self.today, page=self.page)
        yield Request(url=url)

    def parse(self, response):
        links = response.xpath("//article/a/@href").getall()
        for link in links:
            yield Request(url=link, callback=self.parse_article)

        if response.xpath('//div[@class="text_center mt20 mb20"]'):
            self.page += 1
            url = self.base_url.format(date=self.today, page=self.page)
            yield Request(url=url, callback=self.parse)

    def parse_article(self, response):
        title = response.xpath('//h1[@class="title"]/text()').get().strip()
        author, date = (
            response.xpath('//div[@class="date"]/text()').get().strip().split(" | ")
        )
        content = "\n".join(
            response.xpath('//div[@class="detail_text"]//text()').getall()
        ).strip()

        return self.to_item_loader(response, title, content, author, date)
