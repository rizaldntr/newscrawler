# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from typing import Iterable

from scrapy import signals, crawler as crawler_type
from scrapy.http import Response, Request
from scrapy.spiders import Spider


class Kawalcovid19SpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler: crawler_type) -> 'Kawalcovid19SpiderMiddleware':
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    @staticmethod
    def process_spider_input(_response: Response, _spider: Spider) -> None:
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    @staticmethod
    def process_spider_output(_response: Response, result: Iterable, _spider: Spider) -> Iterable:
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    @staticmethod
    def process_spider_exception(_response: Response, _exception: Exception, _spider: Spider) -> None:
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request, dict
        # or Item objects.
        pass

    @staticmethod
    def process_start_requests(start_requests: Iterable, _spider: Spider) -> Iterable:
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    @staticmethod
    def spider_opened(spider: Spider) -> None:
        spider.logger.info("Spider opened: %s" % spider.name)


class Kawalcovid19DownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler: crawler_type) -> 'Kawalcovid19DownloaderMiddleware':
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    @staticmethod
    def process_request(_request: Request, _spider: Spider) -> None:
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    @staticmethod
    def process_response(_request: Request, response: Response, _spider: Spider) -> Response:
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    @staticmethod
    def process_exception(_request: Request, _exception: Exception, _spider: Spider) -> None:
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    @staticmethod
    def spider_opened(spider: Spider) -> None:
        spider.logger.info("Spider opened: %s" % spider.name)
