# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from io import BytesIO

from scrapy.exceptions import DropItem

from kawalcovid19.items import NewsItem
from kawalcovid19.spiders.base import BaseSpider
from kawalcovid19.utils.blob import safe_filename


# Drop the item if the required fields are not exists
class NewsValidationPipeline(object):
    @staticmethod
    def process_item(item: NewsItem, _spider: BaseSpider) -> NewsItem:
        title = item.get("title", "title_not_set")
        if title == "title_not_set":
            err_msg = "Missing title in: %s" % item.get("url")
            raise DropItem(err_msg)

        content = item.get("content", "content_not_set")
        if content == "content_not_set":
            err_msg = "Missing content in: %s" % item.get("url")
            raise DropItem(err_msg)

        published_at = item.get("published_at", "published_at_not_set")
        if published_at == "published_at_not_set":
            err_msg = "Missing published_at in: %s" % item.get("url")
            raise DropItem(err_msg)

        # Pass item to the next pipeline, if any
        return item


# Write every item received from spider to their own corresponding TXT file
class AzureBlobStorageBasicAITXTWriterPipeline(object):
    @staticmethod
    def process_item(item: NewsItem, spider: BaseSpider) -> NewsItem:
        # Create the blob first
        file_id = "{} {}".format(spider.name, item.get("title")[0])
        file_name = safe_filename(file_id + ".txt")
        with BytesIO() as blob:
            blob.write(item.get("title")[0].encode())
            blob.write("\n".encode())
            blob.write(item.get("content")[0].encode())
            # Seek back to 0, so when the blob container upload the content
            # it read from the start
            blob.seek(0)
            spider.azure_basicai_container.upload_blob(
                file_name, blob, overwrite=True
            )
        return item


# Write every item received from the spider to their own corresponding
# JSON file
class AzureBlobStorageJsonWriterPipeline(object):
    @staticmethod
    def process_item(item: NewsItem, spider: BaseSpider) -> NewsItem:
        # Create the blob first
        file_id = "{} {}".format(spider.name, item.get("title")[0])
        file_name = safe_filename(file_id + ".json")
        with BytesIO() as blob:
            blob.write(item.to_json().encode())
            blob.write("\n".encode())
            # Seek back to 0, so when the bldob container upload the content
            # it read from the start
            blob.seek(0)
            spider.azure_json_container.upload_blob(
                file_name, blob, overwrite=True
            )
        return item
